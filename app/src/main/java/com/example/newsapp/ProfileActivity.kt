package com.example.newsapp

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputEditText

class ProfileActivity : AppCompatActivity() {
    private lateinit var emailInput : TextInputEditText
    private lateinit var nameInput : TextInputEditText
    private lateinit var passwordInput : TextInputEditText
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor
    private lateinit var button: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_profile)
        emailInput = findViewById(R.id.email)
        passwordInput = findViewById(R.id.password)
        nameInput = findViewById(R.id.name)
        button = findViewById(R.id.update_btn)

        supportActionBar?.title = "Profile"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
//        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24)

        sharedPreferences = getSharedPreferences(SHAREDPREF, Context.MODE_PRIVATE)

        nameInput.setText(sharedPreferences.getString("name","user"))
        emailInput.setText(sharedPreferences.getString("email","user"))
        passwordInput.setText(sharedPreferences.getString("password","user"))

        button.setOnClickListener {
            editor = sharedPreferences.edit()
            editor.putString("name",nameInput.text.toString())
            editor.putString("email",emailInput.text.toString())
            editor.putString("password",passwordInput.toString())
            editor.commit()
            Toast.makeText(this, "Profile Updated Successfully", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this@ProfileActivity,LandingActivity::class.java))
            finish()
        }

    }


    override fun onBackPressed() {
        super.onBackPressed()
    }
}