package com.example.newsapp

import com.example.newsapp.Models.NewsModel
import retrofit2.Call
import retrofit2.http.GET

interface ApiInterfaces {
    @GET("news?category=all")
    fun getAllNews(): Call<NewsModel>

    @GET("news?category=sports")
    fun getSportsNews(): Call<NewsModel>

    @GET("news?category=politics")
    fun getPoliticsNews(): Call<NewsModel>

    @GET("news?category=world")
    fun getWorldNews(): Call<NewsModel>
}