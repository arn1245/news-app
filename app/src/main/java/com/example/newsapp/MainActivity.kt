package com.example.newsapp

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.example.newsapp.fragments.*
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity() {
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navigationView: NavigationView
    private var backPressedTime:Long = 0
    private var flag = 0
    private lateinit var toolbar: Toolbar
    private var arrayList: ArrayList<Int> = ArrayList()
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var editor : SharedPreferences.Editor
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        drawerLayout = findViewById(R.id.drawer_layout)
        navigationView = findViewById(R.id.navigation_menu)
        toolbar = findViewById(R.id.home_toolbar)
        sharedPreferences = getSharedPreferences(SHAREDPREF,Context.MODE_PRIVATE)
        setSupportActionBar(toolbar)
//        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationIcon(R.drawable.ic_baseline_menu_24)
        toolbar.showOverflowMenu()

        loadFragment(MainFragment(),"Home",0)

        navigationView.setNavigationItemSelectedListener {
            it.isChecked = true

            when(it.itemId){
                R.id.home -> {
                    loadFragment(MainFragment(),"Home",0)
//                    it.isChecked = true

                }
                R.id.sports_news -> {
                    Toast.makeText(this, "Sports News", Toast.LENGTH_SHORT).show()
                    loadFragment(SportsFragment(),"Sports News",1)
                }
                R.id.political_news -> {
                    Toast.makeText(this,"Political News",Toast.LENGTH_SHORT).show()
                    loadFragment(PoliticsFragment(),"Political News",2)
                }
                R.id.world_news -> {
                    Toast.makeText(this,"World News",Toast.LENGTH_SHORT).show()
                    loadFragment(WorldFragment(),"World News",3)
                }
                R.id.logout ->{
//                    Toast.makeText(this,"",Toast.LENGTH_SHORT).show()
                    editor = sharedPreferences.edit()
                    val alertDialogBuilder = AlertDialog.Builder(this@MainActivity)
                    alertDialogBuilder.setMessage("Do you want to logout?")
                    alertDialogBuilder.setPositiveButton("Yes",
                        DialogInterface.OnClickListener { dialog, which ->
                            editor.clear()
                            editor.commit()
                            Toast.makeText(this, "You have been logout", Toast.LENGTH_SHORT).show()
                            startActivity(Intent(this,LoginActivity::class.java))
                            finish()
                        }
                    )
                    alertDialogBuilder.setNegativeButton("No",
                    DialogInterface.OnClickListener { dialog, which ->

                    })
                    alertDialogBuilder.show()
                }
                R.id.navigate->{
                    startActivity(Intent(this@MainActivity,LandingActivity::class.java))
                }
                else -> {
                    Log.d("MENU","Nothing is selected")
                }
            }
            true
        }

    }


    private fun loadFragment(fragment : Fragment, title: String, f:Int){
        flag = f
        arrayList.add(flag)
        supportActionBar?.title = title
//        if(arrayList[arrayList.size-1] != 0){
            supportFragmentManager.beginTransaction().replace(R.id.fragment,fragment,null).addToBackStack(null).commit()
//        }
        if (this.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout.closeDrawer(GravityCompat.START)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                drawerLayout.openDrawer(GravityCompat.START)
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        drawerLayout.openDrawer(navigationView)
        return true
    }

    override fun onPause() {
        Log.d("TAG", "onPause: called")
        super.onPause()
    }

    override fun onResume() {
        onStart() //fixme in lollipop dark theme doesn't work on change theme
        super.onResume()
    }

    override fun onBackPressed() {
        if (this.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout.closeDrawer(GravityCompat.START)
        }
        arrayList.removeLast()
        supportFragmentManager.popBackStack()
        if(arrayList.size > 0){
            when(arrayList[arrayList.size-1]){
                0->{
                    supportActionBar?.title = "Home"
                }
                1->{
                    supportActionBar?.title = "Sports News"
                }
                2->{
                    supportActionBar?.title = "Political News"
                }
                3->{
                    supportActionBar?.title = "World News"
                }
                4->{
                    supportActionBar?.title = "Profile"
                }
            }
        }else{
            if (System.currentTimeMillis() - backPressedTime > 2000) {
                Toast.makeText(this, "Press back again to Exit", Toast.LENGTH_SHORT).show();
                backPressedTime = System.currentTimeMillis();
            }else{
                super.onBackPressed();
            }
        }


//        Toast.makeText(this, arrayList[arrayList.size-1].toString(), Toast.LENGTH_SHORT).show()
    }
}