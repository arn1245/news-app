package com.example.newsapp

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.newsapp.Models.NewsDataModel
import com.bumptech.glide.Glide

class AdapterAll(private var newslist : List<NewsDataModel>): RecyclerView.Adapter<AdapterAll.AllNewsViewHolder>() {
    inner class AllNewsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val imageView: ImageView = itemView.findViewById(R.id.news_image)
        val newsTitleTextView: TextView = itemView.findViewById(R.id.news_title)
        val newsDateTextView: TextView = itemView.findViewById(R.id.news_date)
        val news_card: CardView = itemView.findViewById(R.id.news_card)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllNewsViewHolder {
        val root = LayoutInflater.from(parent.context).inflate(R.layout.news_items_layout,parent,false)
        return AllNewsViewHolder(root)
    }

    override fun onBindViewHolder(holder: AllNewsViewHolder, position: Int) {
        Glide.with(holder.itemView.context).load(newslist[position].imageUrl).placeholder(R.drawable.placeholder_news).into(holder.imageView)
        holder.newsTitleTextView.text = newslist[position].title
        holder.newsDateTextView.text = newslist[position].date
        holder.news_card.setOnClickListener {
            val intent = Intent(holder.itemView.context,NewsActivity::class.java)
            intent.putExtra("image_url",newslist[position].imageUrl)
            intent.putExtra("title",newslist[position].title)
            intent.putExtra("date",newslist[position].date)
            intent.putExtra("author",newslist[position].author)
            intent.putExtra("content",newslist[position].content)
            intent.putExtra("readmore_url",newslist[position].url)
//            Toast.makeText(holder.itemView.context, newslist[position].readMoreUrl.toString(), Toast.LENGTH_SHORT).show()
            holder.itemView.context.startActivity(intent)
            Log.d("URL",newslist[position].url.toString())
        }

    }

    override fun getItemCount(): Int {
        return newslist.size
    }
}