package com.example.newsapp

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.example.newsapp.databinding.ActivityLandingBinding

class LandingActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityLandingBinding
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor
    private lateinit var drawerLayout: DrawerLayout
    private var backPressedTime: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLandingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarLanding.toolbar)
        sharedPreferences = getSharedPreferences(SHAREDPREF, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
//        binding.appBarLanding.fab.setOnClickListener { view ->
//            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show()
//        }
        drawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView

        val nv = navView.getHeaderView(0)

        val emailNavTextView: TextView = nv.findViewById(R.id.user_email)
        val userNavTextView: TextView = nv.findViewById(R.id.user_name)

        emailNavTextView.text = sharedPreferences.getString("email","user@androidstudio.com")
        userNavTextView.text = sharedPreferences.getString("name","USER")

        val navController = findNavController(R.id.nav_host_fragment_content_landing)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.mainFragment, R.id.sportsFragment, R.id.politicsFragment,R.id.worldFragment
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.landing, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home->{
                if (drawerLayout.isDrawerOpen(GravityCompat.START)){
                    drawerLayout.closeDrawer(GravityCompat.START)
                }else{
                    drawerLayout.openDrawer(GravityCompat.START)
                }
                return true
            }
            R.id.action_profile ->{
                startActivity(Intent(this@LandingActivity,ProfileActivity::class.java))
                return true
            }
            R.id.action_logout ->{
                editor = sharedPreferences.edit()
                val alertDialogBuilder = AlertDialog.Builder(this@LandingActivity)
                alertDialogBuilder.setMessage("Do you want to logout?")
                alertDialogBuilder.setPositiveButton("Yes",
                    DialogInterface.OnClickListener { dialog, which ->
                        editor.clear()
                        editor.commit()
                        Toast.makeText(this, "You have been logout", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this,LoginActivity::class.java))
                        finish()
                    }
                )
                alertDialogBuilder.setNegativeButton("No",
                    DialogInterface.OnClickListener { dialog, which ->

                    })
                alertDialogBuilder.show()
                return true
            }
            else->{
                return super.onOptionsItemSelected(item)
            }
        }
        Log.d("MENUITEM",item.toString())
//        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_landing)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        if (System.currentTimeMillis() - backPressedTime > 2000) {
            Toast.makeText(this, "Press back again to Exit", Toast.LENGTH_SHORT).show();
            backPressedTime = System.currentTimeMillis();
        }else{
            super.onBackPressed();
        }
    }
}