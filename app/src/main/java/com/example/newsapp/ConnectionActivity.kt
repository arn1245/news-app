package com.example.newsapp

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button

class ConnectionActivity : AppCompatActivity() {
    private lateinit var retry : Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_connection)
        retry = findViewById(R.id.retry_connection)
        retry.setOnClickListener {
            if(Connection.check()){
                if(Connection.checkLogin()){
                    val goMainActivity = Intent(this,MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }else{
                    val goLoginActivity = Intent(this,LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        }


    }
}