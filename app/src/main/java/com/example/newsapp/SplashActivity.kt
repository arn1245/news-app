package com.example.newsapp

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log

class SplashActivity : AppCompatActivity() {
    private  lateinit var sharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        sharedPreferences = getSharedPreferences(SHAREDPREF,Context.MODE_PRIVATE)

        supportActionBar?.hide()
        Handler().postDelayed({
            //will check for login data & internet connectivity
            if(!check()){
                startActivity(Intent(this,ConnectionActivity::class.java))
                finish()
            }
            if(checkLogin()){
                val intent = Intent(this,LandingActivity::class.java)
                startActivity(intent)
                finish()
            }else{
                val intent = Intent(this,LoginActivity::class.java)
                startActivity(intent)
                finish()
            }

        },3000)

    }
    fun check(): Boolean{
        val cm = baseContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                cm.getNetworkCapabilities(cm.activeNetwork)
            } else {
                TODO("VERSION.SDK_INT < M")
            }
        if (capabilities != null) {
            if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
            }
            return true
        }else{
            return false
        }
    }
    fun checkLogin():Boolean{
        return sharedPreferences.getBoolean("is_login",false)
    }
}