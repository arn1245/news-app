package com.example.newsapp

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.bumptech.glide.Glide

class NewsActivity : AppCompatActivity() {
    private lateinit var newsImage:ImageView
    private lateinit var newsTitle: TextView
    private lateinit var newsDate: TextView
    private  lateinit var newsContent: TextView
    private lateinit var newsAuthor: TextView
    private lateinit var newsReadMore: TextView
    private lateinit var goBackButton: ImageButton
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)
        initView()

        Glide.with(this).load(intent.getStringExtra("image_url")).placeholder(R.drawable.placeholder_news).into(newsImage)
        newsAuthor.text = "Author : ${intent.getStringExtra("author")}"
        newsDate.text = intent.getStringExtra("date")
        newsTitle.text = intent.getStringExtra("title")
        newsContent.text = intent.getStringExtra("content")
        newsReadMore.setOnClickListener {
            val openURL = Intent(Intent.ACTION_VIEW)
            openURL.data = Uri.parse(intent.getStringExtra("readmore_url").toString())
            startActivity(openURL)
            Log.d("URL",intent.getStringExtra("readmore_url").toString())
        }

        goBackButton.setOnClickListener {
            finish()
        }
    }

    private fun initView() {
        newsImage = findViewById(R.id.news_image)
        newsTitle = findViewById(R.id.news_title)
        newsDate = findViewById(R.id.news_date)
        newsContent = findViewById(R.id.content)
        newsAuthor = findViewById(R.id.author_name)
        newsReadMore = findViewById(R.id.readMore)
        goBackButton = findViewById(R.id.go_back)
    }

    override fun onSupportNavigateUp(): Boolean {
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

}