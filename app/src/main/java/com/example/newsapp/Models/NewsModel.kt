package com.example.newsapp.Models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class NewsModel(
    @Expose
    @SerializedName("category")
    val category:String,
    @Expose
    @SerializedName("data")
    val data: List<NewsDataModel>,
    @Expose
    @SerializedName("success")
    val success:Boolean
)
