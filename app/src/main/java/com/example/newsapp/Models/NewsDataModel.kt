package com.example.newsapp.Models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class NewsDataModel(
    @Expose
    @SerializedName("author")
    val author: String,
    @Expose
    @SerializedName("content")
    val content: String,
    @Expose
    @SerializedName("date")
    val date: String,@Expose
    @SerializedName("id")
    val id: String,
    @Expose
    @SerializedName("imageUrl")
    val imageUrl: String,
    @Expose
    @SerializedName("readMoreUrl")
    val readMoreUrl: String,
    @Expose
    @SerializedName("time")
    val time: String,
    @Expose
    @SerializedName("title")
    val title: String,
    @Expose
    @SerializedName("url")
    val url: String,
)
