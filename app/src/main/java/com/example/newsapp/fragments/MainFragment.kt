package com.example.newsapp.fragments

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.newsapp.*
import com.example.newsapp.Models.NewsDataModel
import com.example.newsapp.Models.NewsModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: AdapterAll
    private var newsDataList = ArrayList<NewsDataModel>()
    private lateinit var latestNewsImage: ImageView
    private lateinit var latestNewsDate: TextView
    private lateinit var latestTitle: TextView
    private lateinit var progressBar: ProgressBar
    private lateinit var root: View
    private lateinit var latestNewsView : NestedScrollView
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var userNameTextView: TextView
    var lt_news_img = ""
    var lt_news_title = ""
    var lt_news_date = ""
    var lt_news_author = ""
    var lt_news_content = ""
    var lt_news_url = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        root = inflater.inflate(R.layout.fragment_main, container, false)
        intiView()

        sharedPreferences = root.context.getSharedPreferences(SHAREDPREF,Context.MODE_PRIVATE)
        userNameTextView.text = sharedPreferences.getString("name","User")
        newsDataList.clear()
        adapter = AdapterAll(newsDataList)
        recyclerView.layoutManager = LinearLayoutManager(root.context,LinearLayoutManager.VERTICAL,false)
        recyclerView.adapter = adapter

        val client = RetrofitClient.getInstance().create(ApiInterfaces::class.java)

        val call: Call<NewsModel> = client.getAllNews()
        progressBar.visibility = View.VISIBLE
        latestNewsView.visibility = View.GONE
        call.enqueue(object : Callback<NewsModel>{
            override fun onResponse(
                call: Call<NewsModel>,
                response: Response<NewsModel>
            ) {
                Log.d("onResponse",response.body()!!.data[0].imageUrl)
                progressBar.visibility = View.GONE
                topNews(response.body()!!.data[0].imageUrl,response.body()!!.data[0].date,response.body()!!.data[0].title)
                newsDataList.addAll(response.body()!!.data)
                newsDataList.removeAt(0)
                adapter.notifyDataSetChanged()
                lt_news_author = response.body()!!.data[0].author
                lt_news_img = response.body()!!.data[0].imageUrl
                lt_news_title = response.body()!!.data[0].title
                lt_news_date = response.body()!!.data[0].date
                lt_news_content = response.body()!!.data[0].content
                lt_news_url = response.body()!!.data[0].url

            }

            override fun onFailure(call: Call<NewsModel>, t: Throwable) {
                progressBar.visibility = View.GONE
                Log.e("onFailure",t.message.toString())
            }

        })
        latestNewsImage.setOnClickListener{
            val intent = Intent(root.context,NewsActivity::class.java)
            intent.putExtra("image_url",lt_news_img)
            intent.putExtra("title",lt_news_title)
            intent.putExtra("date",lt_news_date)
            intent.putExtra("author",lt_news_author)
            intent.putExtra("content",lt_news_content)
            intent.putExtra("readmore_url",lt_news_url)
            root.context.startActivity(intent)
        }

        return root;
    }

    private fun topNews(imageUrl: String, date: String, title: String) {
        Glide.with(root).load(imageUrl).placeholder(R.drawable.placeholder_news).into(latestNewsImage)
        latestTitle.text = title
        latestTitle.paintFlags = latestTitle.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        latestNewsDate.text = date
        latestNewsView.visibility = View.VISIBLE
    }

    private fun intiView() {
        recyclerView = root.findViewById(R.id.recyclerview_all)
        latestNewsImage = root.findViewById(R.id.latest_image)
        latestNewsDate = root.findViewById(R.id.latest_date)
        progressBar = root.findViewById(R.id.progress_all)
        latestTitle = root.findViewById(R.id.latest_title)
        latestNewsView = root.findViewById(R.id.latest_news)
        userNameTextView = root.findViewById(R.id.user_name)
    }

}