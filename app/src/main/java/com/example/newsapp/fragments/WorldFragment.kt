package com.example.newsapp.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.newsapp.AdapterAll
import com.example.newsapp.ApiInterfaces
import com.example.newsapp.Models.NewsDataModel
import com.example.newsapp.Models.NewsModel
import com.example.newsapp.R
import com.example.newsapp.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WorldFragment : Fragment() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: AdapterAll
    private var newsDataList = ArrayList<NewsDataModel>()
    private lateinit var root: View
    private lateinit var progressBar: ProgressBar
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_world, container, false)
        initView()
        newsDataList.clear()
        adapter = AdapterAll(newsDataList)
        recyclerView.layoutManager = LinearLayoutManager(root.context,
            LinearLayoutManager.VERTICAL,false)
        recyclerView.adapter = adapter

        val client = RetrofitClient.getInstance().create(ApiInterfaces::class.java)
        progressBar.visibility = View.VISIBLE
        val call: Call<NewsModel> = client.getWorldNews()
        call.enqueue(object : Callback<NewsModel> {
            override fun onResponse(
                call: Call<NewsModel>,
                response: Response<NewsModel>
            ) {
                progressBar.visibility = View.GONE
//                Log.d("onResponse",response.body()!!.data[0].imageUrl)
                newsDataList.addAll(response.body()!!.data)
                adapter.notifyDataSetChanged()
            }

            override fun onFailure(call: Call<NewsModel>, t: Throwable) {
                progressBar.visibility = View.GONE
                Log.e("onFailure",t.message.toString())
            }

        })

        return root
    }

    private fun initView() {
        recyclerView = root.findViewById(R.id.recyclerview_world)
        progressBar = root.findViewById(R.id.progress_world)
    }

}