package com.example.newsapp

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ProgressBar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import org.w3c.dom.Text

class LoginActivity : AppCompatActivity() {
    private lateinit var name_TextInut : TextInputLayout
    private lateinit var email_TextInut : TextInputLayout
    private lateinit var password_TextInut : TextInputLayout
    private lateinit var loginBtn: Button
    private lateinit var progressBar : ProgressBar
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        name_TextInut = findViewById(R.id.name)
        email_TextInut = findViewById(R.id.email)
        password_TextInut = findViewById(R.id.password)
        loginBtn = findViewById(R.id.login_btn)
        progressBar = findViewById(R.id.progress_bar)

        sharedPreferences = getSharedPreferences(SHAREDPREF,Context.MODE_PRIVATE)

        supportActionBar?.hide()


            loginBtn.setOnClickListener {
                if(name_TextInut.editText?.text.isNullOrEmpty()){
                    name_TextInut.editText?.error = "Name is required"
                    name_TextInut.editText?.isFocusable = false
                    return@setOnClickListener
                }else
                if(email_TextInut.editText?.text.isNullOrEmpty()){
                    email_TextInut.editText?.error = "Email is required"
                    email_TextInut.editText?.isFocusable = false
                    return@setOnClickListener
                }else
                if(password_TextInut.editText?.text.isNullOrEmpty()){
                    password_TextInut.editText?.error = "Password is required"
                    password_TextInut.editText?.isFocusable = false
                    return@setOnClickListener
                }else{
                    editor = sharedPreferences.edit()
                    editor.putString("name",name_TextInut.editText?.text.toString())
                    editor.putString("email",email_TextInut.editText?.text.toString())
                    editor.putString("password",password_TextInut.editText?.text.toString())
                    editor.putBoolean("is_login",true)
                    editor.commit()
                    startActivity(Intent(this,LandingActivity::class.java))
                    finish()
                }
            }

    }

}